import { of } from 'rxjs';

const MOCK_DATA = [{ id: 1 }, { id: 2 }];

class MockUserService {
  getUsers() {
    return of(MOCK_DATA);
  }
}

export { MockUserService, MOCK_DATA };
