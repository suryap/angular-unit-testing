import { TestBed, inject } from '@angular/core/testing';

import { UserService } from './user.service';
import { environment } from '../../environments/environment';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';

describe('UserService', () => {
  let service: UserService;
  let httpMock: HttpTestingController;
  const mockResponse = [
    {
      id: 1
    },
    {
      id: 2
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    httpMock = TestBed.get(HttpTestingController);
    service = TestBed.get(UserService);
  });

  afterEach(inject(
    [HttpTestingController],
    (httpMock: HttpTestingController) => {
      httpMock.verify();
    }
  ));

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('.getUsers()', () => {
    it('should get all the users from typicode service', () => {
      service.getUsers().subscribe((response: any[]) => {
        expect(response).toBeTruthy();
        expect(response.length).toBe(mockResponse.length);
      });

      const url = `${environment.apiURL}users`;
      const testRequest = httpMock.expectOne(url);

      expect(testRequest.request.method).toEqual('GET');
      testRequest.flush(mockResponse);
    });
  });
});
