import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { catchError, map, mergeMap, tap } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  url = environment.apiURL;
  constructor(private http: HttpClient) {}

  getUsers() {
    return this.http.get(`${this.url}users`).pipe(
      catchError(error => {
        // log here
        return throwError(error);
      })
    );
  }
}
