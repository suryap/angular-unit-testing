import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersComponent } from './users.component';
import { UserService } from '../services/user.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { MockUserService, MOCK_DATA } from 'src/mocks/user.service-mock';

fdescribe('UsersComponent', () => {
  let component: UsersComponent;
  let fixture: ComponentFixture<UsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [UsersComponent],
      providers: [
        {
          provide: UserService,
          useClass: MockUserService
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call getUsers method on initialization', () => {
    const uService = TestBed.get(UserService);
    spyOn(uService, 'getUsers').and.callThrough();

    component.ngOnInit();
    expect(uService.getUsers).toHaveBeenCalled();
    expect(component.users).toBe(MOCK_DATA);
  });
});
