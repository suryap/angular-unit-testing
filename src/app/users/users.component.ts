import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { Subject } from 'rxjs';
import { tap, take, debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  isLoading: boolean = false;
  users = [];
  destroy$ = new Subject();
  constructor(private userService: UserService) {}

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    this.isLoading = true;
    this.userService
      .getUsers()
      .pipe(
        take(1),
        tap(() => (this.isLoading = false))
      )
      .subscribe((response: any[]) => (this.users = response));
  }
}
